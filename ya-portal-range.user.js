// ==UserScript==
// @id             iitc-plugin-yet_another_portal_range
// @name           IITC plugin: Yet Another Portal Range
// @category       Layer
// @version        0.0.1
// @updateURL      https://gitlab.com/pwiecz/yet-another-portal-range/raw/master/ya-portal-range.user.js
// @downloadURL    https://gitlab.com/pwiecz/yet-another-portal-range/raw/master/ya-portal-range.user.js
// @description    Add a circle around the portals and markers at some random distance
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

function wrapper(){
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function(){};

// PLUGIN START ////////////////////////////////////////////////////////

	// use own namespace for plugin
	window.plugin.yarange = function() {};
	window.plugin.yarange.yaLayers = {};
	window.plugin.yarange.MIN_MAP_ZOOM = 17;

	window.plugin.yarange.removeCircle = function(guid){
		var previousLayer = window.plugin.yarange.yaLayers[guid];
		if(previousLayer){
			window.plugin.yarange.yaCircleHolderGroup.removeLayer(previousLayer);
			delete window.plugin.yarange.yaLayers[guid];
		}
	}
    window.plugin.yarange.drawCircle = function(latlng) {
        var optCircle = {color:'red',opacity:1,fillColor:'red',fillOpacity:0.2,weight:1,clickable:false, dashArray: [5,3]};
		var range = window.HACK_RANGE/2;

		var circle = new L.Circle(latlng, range, optCircle);
		circle.addTo(window.plugin.yarange.yaCircleHolderGroup);
        return circle;
    }
	window.plugin.yarange.addCircle = function(guid){
		var d = window.portals[guid];
		var coo = d._latlng;
		var latlng = new L.LatLng(coo.lat,coo.lng);
        var circle = window.plugin.yarange.drawCircle(latlng);
		window.plugin.yarange.yaLayers[guid] = circle;
	}
    window.plugin.yarange.addMarkerCircle = function(marker) {
        var latlng = marker.getLatLng();
        var circle = window.plugin.yarange.drawCircle(latlng);
        window.plugin.yarange.yaLayers[latlng] = circle;
    }
    window.plugin.yarange.removeMarkerCircle = function(marker) {
        var latlng = marker.getLatLng();
        var previousLayer = window.plugin.yarange.yaLayers[latlng];
        if(previousLayer){
            window.plugin.yarange.yaCircleHolderGroup.removeLayer(previousLayer);
            delete window.plugin.yarange.yaLayers[latlng];
        }
    }
	window.plugin.yarange.portalAdded = function(data){
		data.portal.on('add', function(){
			window.plugin.yarange.addCircle(this.options.guid);
		});

		data.portal.on('remove', function(){
			window.plugin.yarange.removeCircle(this.options.guid);
		});
	}
    window.plugin.yarange.featureDrawn = function(e){
        if (e.layerType != L.Draw.Marker.TYPE) {
            return;
        }
        window.plugin.yarange.addMarkerCircle(e.layer);
    }
    window.plugin.yarange.featureDeleted = function(e){
        e.layers.eachLayer(function(layer) {
            if (layer instanceof L.Marker) {
                window.plugin.yarange.removeMarkerCircle(layer);
            }
        });
    }

	// *****************************************************************

	var setup =  function() {
		// this layer is added to the layer chooser, to be toggled on/off
		window.plugin.yarange.rangeLayerGroup = new L.LayerGroup();
		// this layer is added into the above layer, and removed from it when we zoom out too far
		window.plugin.yarange.yaCircleHolderGroup = new L.LayerGroup();

		window.plugin.yarange.rangeLayerGroup.addLayer(window.plugin.yarange.yaCircleHolderGroup);

		window.addLayerGroup('YA Portal Ranges', window.plugin.yarange.rangeLayerGroup, true);

		window.addHook('portalAdded', window.plugin.yarange.portalAdded);

        map.on('draw:created', window.plugin.yarange.featureDrawn);
        map.on('draw:deleted', window.plugin.yarange.featureDeleted);
        if (window.plugin.drawTools) {
            window.plugin.drawTools.drawnItems.eachLayer( function(layer) {
                if (layer instanceof L.Marker) {
                    window.plugin.yarange.addMarkerCircle(layer);
                }
            });
        }
	}

// PLUGIN END //////////////////////////////////////////////////////////

if(window.iitcLoaded && typeof setup === 'function'){
	setup();
}else{
	if(window.bootPlugins)
		window.bootPlugins.push(setup);
	else
		window.bootPlugins = [setup];
}
} // wrapper end
// inject code into site context
var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);
